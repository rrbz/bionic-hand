---
title: La nostra squadra
subtitle: About us
comments: false
---

#### Ciao a tutti!
Noi siamo Alessia, Andrea, Carlo, Davide, Federica, Leonardo, Luca, Mario, Niccolò, Patrick, Samuele, Thomas e siamo la squadra "I TUXXINI" del Liceo Scientifico Rainerum di Bolzano.  
Facciamo tutti parte della classe III e abbiamo moltissimi interessi diversi.

---

#### Hi everybody!
We are Alessia, Andrea, Carlo, Davide, Federica, Leonardo, Luca, Mario, Niccolò, Patrick, Samuele, Thomas and we are the members of the team "I TUXXINI", from the scientific high school Rainerum in Bolzano.   
We are all students of the III year and have a lot of different interests.
