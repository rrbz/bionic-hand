---
title: Presentazione
subtitle: La scuola, la squadra e l'ente
date: 2021-03-08
bigimg: [{src: "/img/hexagon.jpg"}]
draft: false
---

{{< youtube id="5f9zbn2spRw" >}}

Abbiamo realizzato questo video, in cui ci presentiamo, presentiamo la nostra scuola e l'ente con cui abbiamo scelto di collaborare: **il Centro culturale Trevi**. 
Il video si trova sul nostro [canale YouTube](https://www.youtube.com/channel/UClKC7Y-IXTgcg5dYJvOBkxQ).
Il video è stato montato da Andrea e ha partecipato quasi tutta la squadra.

We have realized this video, in which we present our team, our school and the institution, with which we decided to collaborate: **the Trevi center**.
The video can be found on our [YouTube channel](https://www.youtube.com/channel/UClKC7Y-IXTgcg5dYJvOBkxQ).
The video was edited by Andrea and almost the whole team took part to its production.
