---
title: Pixel Recreation Challenge
date: 2021-01-13
---
{{< gviewer 1i0kTYxfVQPRsgTlg590cAqe0oGhZnW84Xir2nJB6CyA >}}

Alcuni componenti della squadra dei TUXXINI si sono impegnati a completare la "pixel-challenge", disegnando la "Monna Lisa", un unicorno e un robot, utilizzando un foglio condiviso su Google Drive.

A few members of the Tuxxini Team have completed the "pixel-challenge", by drawing the "Mona Lisa", a unicorn and a robot, using a shared spreadsheet on Google Drive.

[]: # (Einige Mitglieder der Tuxxini Gruppe haben die "Pixel-Challenge" fertiggestellt. Wir haben die "Mona Lisa", ein Einhorn und ein Roboter gezeichnet, auf einem geteilten Blatt auf Google Drive.)
