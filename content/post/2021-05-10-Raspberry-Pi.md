---
title: Il Raspberry Pi e i sensori esterni
date: 2021-03-08
draft: false
---

Come sensori esterni da utilizzare con il NAO abbiamo optato per dei sensori di rilevamento del movimento a infrarossi HC-SR501.
Per far interfacciare questi sensori con il robot NAO abbiamo scelto di utilizzare un computer Raspberry Pi. Si tratta di un Single Board Computer (SBC) programmabile con Python e dispone di una serie di GPIO, attraverso cui si possono collegare facilmente i sensori.

Di seguito si trova il codice che usa il RasPi per comunicare con il NAO ed il sensore.

```python
#!/bin/python3

import time
import RPi.GPIO as GPIO
from socket import *

GPIO.setmode(GPIO.BOARD)
pin = 7
GPIO.setup(pin, GPIO.IN)

sock = socket(AF_INET, SOCK_STREAM)
sock.connect(('192.168.181.201', 8080))

print('Waiting for sensor to settle...')
time.sleep(2)
print('Detecting motion.')

try:
    while True:
        if GPIO.input(pin):
	    print('Motion detected.')
            sock.send('Per favore, allontanati dalla statua.'.encode('utf-8'))
except KeyboardInterrupt:
    pass

sock.close()	
```


