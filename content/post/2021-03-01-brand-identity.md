---
title: Brand Identity
subtitle: Il nostro logo
date: 2021-03-01
---

[Qui](/img/Palette.pdf) potete trovare la tavola minima con palette dei colori e dei font.

Di seguito il nostro logo, creato da Alessia:
{{< gallery >}}
{{< figure link="/img/tuxxini-icon.png" >}}
{{< /gallery >}}
